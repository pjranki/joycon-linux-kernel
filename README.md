# HID driver (linux kernel) for Nintendo Joy-Con peripherals
Copyright (c) 2018 Peter Rankin rankstar59@gmail.com

## GPL
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

## Use
Driver starts in "combine" mode where both the left and right
controller inputs are directed to the left controller.
Pressing the MODE/NFC button on the left controller changes
it to "indiviual" mode where each controler has its own buttons
and layout changes to horizonal (e.g. you press SL/SR instead
of L/R). By turning off both controllers, it returns to
"combine" mode.
